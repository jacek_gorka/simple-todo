Simple-todo app...

    - create list
    - create task in list
    - share lists
    

    ----------------------------------------------------------------- 
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Welcome to your Meteor project on Cloud9 IDE!

To run your new app open your terminal and type:
   
    $ meteor --port $IP:$PORT


Happy coding!
The Cloud9 IDE team

## Support & Documentation

Meteor documentation can be found at http://docs.meteor.com/